(function($){
    $(function(){
  
      let sbox = $('#search_box');
      if (sbox) {
        sbox.keypress(function() {
            //console.log( "Handler for .keypress() called.", sbox );
            // Declare variables 
            var filter, table, tr, td, i;
            filter = $(sbox).val().toUpperCase();
            table = document.getElementById("aTable");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
                } 
            }
          });
      }
      

    }); // end of document ready
  })(jQuery); // end of jQuery name space
