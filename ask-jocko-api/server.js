var express    = require('express');        
var app        = express();                 
var bodyParser = require('body-parser');
var apiHelpers  = require('./apiHelpers');
var apiOptions  = require('./apiOptions');
var dataService = new apiHelpers();
var dataOptions = new apiOptions();
var request = require('request');
var cors = require('express-cors');

app.use(cors({
    allowedOrigins: [
        'gitlab.com', 'askjocko.com', 'localhost:3000'
    ]
}))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router


// *** routes for API go here ****

//search posts
router.route('/search/:searchString?') //the ? makes the parameter optional, so express doesn't throw an error
    .get(function(req,res){

        let searchString = req.params.searchString;
        let query = dataOptions.searchQuotedTweets(searchString);
        let fields = dataOptions.fields;

        if (searchString) {
            dataService.searchTweets(query, fields).then(function (docs) {
                    res.send(docs);
        })
        }else{
            res.send('You did not provide a search query. Good.')  
        }
    });

router.route('/getAllTweets') //the ? makes the parameter optional, so express doesn't throw an error
    .get(function(req,res){
        
        let query = dataOptions.getAllTweets();
        let fields = dataOptions.fields;
        
        dataService.searchTweets(query, fields).then(function (docs) {
                res.send(docs);
        })
    });

//get posts by Id
router.route('/post/:Id?')
.get(function(req,res){

    let Id = req.params.Id;

    if (Id) {
                res.send('you are looking up this id ' + Id);
    }else{
        res.send('You did not search anything. Good.')
    }
});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api

//app.use(cors());

app.use('/api', router);


// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Getting after it on port ' + port);
