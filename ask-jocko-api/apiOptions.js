'use strict';
var request = require('request');


let apiOptions = function Constructor() {
    this.searchQuotedTweets = function (searchString){
        return {
            "in_reply_to_user_id": null, 
            "quoted_status_id": {$exists: true}, 
            "text" : new RegExp(searchString, 'i')} //subsring search, case insensitive
    },
    this.fields = 
        {id_str: 1, text: 1, "quoted_status.text": 1, "quoted_status.user":1},
    this.getAllTweets = function(){
        return {
            "in_reply_to_user_id": null, 
            "quoted_status_id": {$exists: true}
        }
    }
}

module.exports = apiOptions;


/*
//old way
var items = ['item1', 'item2', 'item3'];
var copy = [];

for (var i=0; i<items.length; i++) {
  copy.push(items[i])
}

//for each way  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach
//also https://appendto.com/2016/05/iterating-javascript-arrays-with-array-foreach/
var items = ['item1', 'item2', 'item3'];
var copy = [];

items.forEach(function(item){
  copy.push(item)
});

*/

