'use strict';
var dbconn = require('./db.js');
const tweets = dbconn.get('jockotweets');

let apiHelpers = function Constructor() {
    this.searchTweets= function(searchType, fields) {      
        return tweets.find(searchType,fields)      
    }
}

module.exports = apiHelpers