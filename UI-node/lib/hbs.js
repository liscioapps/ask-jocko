var hbs = require('hbs');
var getUrls = require('get-urls');

hbs.registerHelper('removeURLS', function(context, options) {
    let theText = '';

    if (context) {
        theText = context.toString();
        let urlSet = getUrls(theText);
    
        urlSet.forEach(function(value) {
            theText = theText.replace(value, '')
          });
    }


    return theText;
});

module.exports = hbs;