var request = require('request');

if (!process.env.API_SERVICE_HOST) {
    console.log("No API service host, running home to localhost");
    process.env.API_SERVICE_HOST = "localhost"
}

if (!process.env.API_SERVICE_PORT) {
    process.env.API_SERVICE_PORT = "8080"
}

let url = 'http://' + process.env.API_SERVICE_HOST + ':' + process.env.API_SERVICE_PORT +'/api/getalltweets'
console.log("UI/API I think is here: ", url);

function getalltweets(cb) {
    request(url, cb);
}

module.exports = getalltweets;