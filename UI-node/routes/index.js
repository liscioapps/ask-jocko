var express = require('express');
var router = express.Router();
var fake = require('../lib/fakedata');
var data = require('../lib/data.js');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    title: "AskJocko.com",
    tweets: fake
  });
});

router.get('/real', function(req, res, next) {
  data(function(err, resp, body) {
    theTweets = [
      {text: "Loading tweets..."}
    ]
    try {
      theTweets = JSON.parse(body);
    }
    finally {
      console.log(theTweets.lenght);
      res.render('index', { 
        title: 'AskJocko.com',
        tweets: theTweets
      });
    }
  })

  
});

router.get('/foo', function(req, res, next) {
  res.render('test', {
    title: "TRUCKS",
    tweets: []
  })
})

module.exports = router;
