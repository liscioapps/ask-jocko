var Twit = require('twit');

if(!process.env.TWITTER_CONSUMER_KEY) {
    var env = require('../.env.js');
}

// FULL ACCESS to @your_user
/*
var T = new Twit({
    consumer_key:         process.env.TWITTER_CONSUMER_KEY,
    consumer_secret:      process.env.TWITTER_CONSUMER_SECRET,
    access_token:         process.env.TWITTER_ACCESS_TOKEN,
    access_token_secret:  process.env.TWITTER_ACCESS_SECRET,
    timeout_ms:           60*1000,  // optional HTTP request timeout to apply to all requests.
});
*/

console.log("Consumer key: ", process.env.TWITTER_CONSUMER_KEY);
console.log("Consumer secret: ", process.env.TWITTER_CONSUMER_SECRET);

// APP level access only
var Tapponly = new Twit({
    consumer_key:         process.env.TWITTER_CONSUMER_KEY,
    consumer_secret:      process.env.TWITTER_CONSUMER_SECRET,
    app_only_auth:        true, 
    timeout_ms:           60*1000,  // optional HTTP request timeout to apply to all requests.
});

module.exports = Tapponly;