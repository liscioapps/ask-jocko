var debugTime = false;

if ((process.env.NODE_ENV || 'development') === 'development') {
    debugTime = true;
}

var twit = require('./twit');
var dbconn = require('./db.js');
var errprev = require('./error-prevention');

let core = function Constructor() {
    this.debugTime = debugTime;
    this.twitter = twit;
    this.db = dbconn;
    this.checks = new errprev();

    this.getTweets = (coll, twitter) => 
    {
        coll.aggregate([
            { $group : {
            _id : null,
            idmin : { $min : "$id" }
            }}
        ]).then((res) => {
            let search = { screen_name: 'jockowillink' };
        
            if (res.length > 0) {
                search.max_id = res[0].idmin;
                console.log("Last Tweet ID: " + res[0].idmin);
            }
            twitter.get('statuses/user_timeline', search, function(err, data, response) {
                if (err) { console.error(err); }
                if (data) {
                    data.forEach(function(element) {
                        coll.update(element, element, {upsert: true});
                        }, this);
                }
            });
        });
    }
}

module.exports = core;