var core = require('./lib/core');
var service = new core();
var assert = require('assert');

console.log('Twitter Serivce initializing');

const tweets = service.db.get('jockotweets');
tweets.createIndex({ id: 1 }, { unique: true });
//.find({"in_reply_to_user_id": null, "quoted_status_id": {$exists: true}})


function loadtweets() {
  // Get last 20 tweets 100 times.
  (function(count) {
    if (count < 100) {
        // call the function.
        service.getTweets(tweets, service.twitter);

        // The currently executing function which is an anonymous function.
        var caller = arguments.callee; 
        setTimeout(function() {
            caller(count + 1);
        }, 1000);    
    }
  })(0);
}

loadtweets();
